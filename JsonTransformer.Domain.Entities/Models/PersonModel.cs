﻿namespace JsonTranformer.Domain.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class PersonModel
    {
        public string primerNombre { get; set; }
        public string segundoNombre { get; set; }
        public string apellidos { get; set; }
        public int propietarioId { get; set; }
    }
}
