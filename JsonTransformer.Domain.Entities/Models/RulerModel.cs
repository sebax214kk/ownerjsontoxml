﻿namespace JsonTranformer.Domain.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class RulerModel
    {
        public int RulerId { get; set; }
        public string Nombre { get; set; }
        public bool Valor { get; set; }
    }
}
