﻿namespace JsonTranformer.Domain.Entities.Models
{
    public class MailConfigurationModel
    {
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUserName { get; set; }
        public string SmtpPassword { get; set; }
        public string Sender { get; set; }
        public string SenderName { get; set; }
        public bool EnableSsl { get; set; }
        public int SmtpDeliveryMethod { get; set; }

    }
}
