﻿namespace JsonTranformer.Domain.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class OwnerModel
    {
        public string identificacion { get; set; }
        public int digitoVerificacionIdentificacion { get; set; }
        public int tipoIdentificacion { get; set; }
        public string nombreTipoIdentificacion { get; set; }
        public string razonSocial { get; set; }
        public int tipoPersona { get; set; }
        public string nombreTipoPersona { get; set; }
        public string correo { get; set; }
        public LocationModel ubicacion { get; set; }
        public PersonModel persona { get; set; }
        public List<VehicleModel> vehiculos { get; set; }

    }
}
