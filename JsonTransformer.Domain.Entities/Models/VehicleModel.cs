﻿namespace JsonTranformer.Domain.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class VehicleModel
    {
        public string placa { get; set; }
        public string modelo { get; set; }
        public string informaqcionAdicionalJson { get; set; }
        public List<AdditionalInformationModel> informacionAdicional { get; set; }
        public int propietarioId { get; set; }
    }
}
