﻿namespace JsonTranformer.Domain.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AdditionalInformationModel
    {
        public string nombre { get; set; }
        public string tipo { get; set; }
        public string valor { get; set; }
    }
}
