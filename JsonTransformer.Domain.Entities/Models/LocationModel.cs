﻿namespace JsonTranformer.Domain.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class LocationModel
    {
        public string pais { get; set; }
        public string ciudad { get; set; }
        public string departamento { get; set; }
        public string barrio { get; set; }
        public string direccion { get; set; }
        public int propietarioId { get; set; }      
    }
}
