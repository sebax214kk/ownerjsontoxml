﻿using System.ComponentModel;

namespace JsonTransformer.Domain.Entities.Enum
{
    public enum ErrorMessagesEnum
    {
        [Description("Tenemos problemas con nuestros servicios, inténtelo de nuevo más tarde.")]
        UnexpectedError = 1,
        [Description("Valor no puede ser vacio")]
        ValueCanNotBeNull = 2,
        [Description("Valor no puede ser null")]
        ValueCanNotBeEmpty = 3,
        [Description("No existen más páginas para mostrar")]
        OwnerNoFoundError = 4,
        [Description("No se logro registrar la nueva regla, inténtelo de nuevo más tarde")]
        RulerInsertError = 5,
        [Description("Nose logro eliminar las reglas del sistema, inténtelo de nuevo más tarde")]
        RulerDeleteError = 6,
        [Description("Ocurrio un error inesperado al intentar guardar el Json ingresado")]
        TransformerJsonError = 7,
        [Description("No se logro enviar el email debido a un error inesperado, inténtelo de nuevo más tarde ")]
        SendEmailError = 8,
        [Description("El texto ingresado no corresponde a un Json valido")]
        JsonInvalid = 9,
        [Description("Ocurrio un error al intentar serializar el objeto Regla")]
        JsonInvalidRuler = 10,
        [Description("No existen propietarios registrados")]
        ValueCanNotOwner = 11,
        [Description("Ocurrio un error al convertir el Propietario a formato XML")]
        XmlToObjectError = 12,
        [Description("No se encontro el propietario")]
        GetOwnerError = 13,
        [Description("No se encontraron reglas")]
        RulerNoFound = 14,

    }

    public enum DocumentTypeEnum
    {
        Cedula = 1,
        Nit = 2
    }

    public enum PersonTypeEnum
    {
        Natural = 1,
        Juridica = 2
    }

    public static partial class ErrorMessageEnumExtention
    {
        public static string ToDescriptionString(this ErrorMessagesEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
