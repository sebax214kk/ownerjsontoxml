﻿using System;
using System.Resources;
using System.Runtime.Serialization;
using JsonTransformer.Domain.Entities.Enum;

[assembly: NeutralResourcesLanguage("es")]
namespace JsonTransformer.Domain.Entities.Helper
{


    [Serializable]
    public class TransformerException : Exception
    {
        public TransformerException() : base() { }

        public TransformerException(string message) : base(message)
        {
        }

        /// <summary>
        /// Thrown a new TransformerException with a message for the user
        /// </summary>
        /// <param name="methodException">Current exception method</param>
        /// <param name="userExceptionMessage">Message to throw to the user</param>
        /// <param name="ex">Exception when ocurrs</param>
        public TransformerException(string methodException, ErrorMessagesEnum userExceptionMessage, Exception ex = null)
        {
            if (ex != null)
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {ex.ToString()}");
            }
            else
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {userExceptionMessage.ToDescriptionString()}");
            }

            throw new TransformerException(userExceptionMessage.ToDescriptionString());
        }

        protected TransformerException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
