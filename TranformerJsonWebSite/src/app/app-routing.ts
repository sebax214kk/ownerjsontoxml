import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components Import
import { TranformerJsonComponent } from './tranformer-json/tranformer-json.component';
import { AppComponent } from './app.component';
import { OwnerRecordsComponent } from './owner-records/owner-records.component';

const appRoutes: Routes = [
    {path: '', component: TranformerJsonComponent},
    {path: 'ownerAll', component: OwnerRecordsComponent}
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
