
import { ErrorMessageHelper } from '../Helper/errorMessageHelper';
import { RulerModel } from '../Models/RulerModel';

export function getErrorMessageHelperInstance() {
    return new ErrorMessageHelper();
}

export function getRulerModelInstance() {
  return new RulerModel();
}
