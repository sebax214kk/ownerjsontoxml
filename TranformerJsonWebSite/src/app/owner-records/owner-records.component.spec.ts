import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerRecordsComponent } from './owner-records.component';

describe('OwnerRecordsComponent', () => {
  let component: OwnerRecordsComponent;
  let fixture: ComponentFixture<OwnerRecordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerRecordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
