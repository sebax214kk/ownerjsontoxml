import { Component, OnInit } from '@angular/core';
import { ToasterService } from 'angular2-toaster';
import { ConectorApiHelper } from '../Core/Helper/conectroApiHelper';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-owner-records',
  templateUrl: './owner-records.component.html',
  styleUrls: ['./owner-records.component.css']
})
export class OwnerRecordsComponent implements OnInit {

  public pageIndex = 1;
  public ownerRecord: any[] = [];
  public loading = false;
  private toasterService: ToasterService;

  constructor(private conectorApiHelper: ConectorApiHelper,
    toasterService: ToasterService) {
      this.toasterService = toasterService;
      this.GetOwnerRecords();
  }

  ngOnInit() {
  }

  public GetOwnerRecords() {
    this.loading = true;
    const url = environment.GetAllOwners
      .replace('[rowCountPerPage]', environment.rowCountPerPage)
      .replace('[pageIndex]', this.pageIndex.toString());
      this.conectorApiHelper.ResolverPeticionGet(url).then(response => {
        this.loading = false;
        this.ownerRecord = this.conectorApiHelper.ObtenerBodyRespuesta(response);
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('warning', 'Atención', errorMessagge);
    });
  }

  public notifyOwner(identification) {
    this.loading = true;
    this.conectorApiHelper.ResolverPeticionGet(environment.SendEmail + identification).then(response => {
      this.loading = false;
      const data = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      if (data) {
        this.toasterService.pop('success', 'Felicidades ¡¡¡', 'Se ha enviado un email con los datos en formato XML');
      }
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('warning', 'Atención', errorMessagge);
    });
  }

  public SearchPrevious() {
    if (this.pageIndex > 1 ) {
      this.pageIndex = this.pageIndex - 1;
      this.GetOwnerRecords();
    }
  }

  public SearchNext() {
    this.pageIndex = this.pageIndex + 1;
    this.GetOwnerRecords();
  }

}
