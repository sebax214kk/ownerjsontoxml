import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TranformerJsonComponent } from './tranformer-json.component';

describe('TranformerJsonComponent', () => {
  let component: TranformerJsonComponent;
  let fixture: ComponentFixture<TranformerJsonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TranformerJsonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TranformerJsonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
