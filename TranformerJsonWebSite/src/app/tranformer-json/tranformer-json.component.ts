import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ErrorMessageHelper } from '../Core/Helper/errorMessageHelper';
import { ToasterService } from 'angular2-toaster';
import { ConectorApiHelper } from '../Core/Helper/conectroApiHelper';
import { getErrorMessageHelperInstance, getRulerModelInstance } from '../Core/Factory/ProjectFactory';
import { environment } from 'src/environments/environment';
import { RulerModel } from '../Core/Models/RulerModel';
import { Router } from '@angular/router';


@Component({
  selector: 'app-tranformer-json',
  templateUrl: './tranformer-json.component.html',
  styleUrls: ['./tranformer-json.component.css']
})
export class TranformerJsonComponent implements OnInit {

  @ViewChild('status') status: ElementRef;


  public TransformerJsonForm: FormGroup;
  public RulerForm: FormGroup;

  public errorMessageHelper: ErrorMessageHelper;
  public currentGamePlayId: number;
  public loading = false;
  private toasterService: ToasterService;
  public jsonText;
  public showNewRuler;
  public listStatusRuler;
  public listNameRuler;
  public rulerOne: RulerModel;

  constructor(private conectorApiHelper: ConectorApiHelper,
    toasterService: ToasterService,
    private router: Router) {
    this.toasterService = toasterService;
    this.errorMessageHelper = getErrorMessageHelperInstance();
    this.rulerOne = getRulerModelInstance();
  }

  ngOnInit() {
    this.InitialiceFormGroup();
    this.DisableSubmitButtonTransformerJsonForm();
    this.DisableSubmitButtonRulerForm();
  }

  private InitialiceFormGroup() {
    this.TransformerJsonForm = new FormGroup({
      jsonText: new FormControl(
        this.jsonText,
        [
          Validators.required
        ]
      )
    });

    this.RulerForm = new FormGroup({
      rulerName: new FormControl(
        this.rulerOne.Name,
        [
          Validators.required
        ]
      ),
      rulerStatus: new FormControl(
        this.rulerOne.Status,
        [
          Validators.required
        ]
      )
    });
    this.showNewRuler = false;
    this.getListStatus();
    this. getListNamesRuler();
  }

  public showFormRuler () {
    this.showNewRuler = true;
  }

 public navigateOwnerRecords() {
   console.log('entro berriondooooo');
  this.router.navigate(['ownerAll']);
 }

  private getListStatus() {
    const json = [
      { 'Valor': 'false', 'Descripcion': 'No' }
    ];
    this.listStatusRuler = json;
    this.listStatusRuler.splice(0, 0, {
      'Valor': 'true', 'Descripcion': 'Si'
    });
  }

  private getListNamesRuler() {
    const json = [
      { 'Valor': 'identificacion', 'Descripcion': 'identificacion' },
      { 'Valor': 'digitoVerificacionIdentificacion', 'Descripcion': 'digitoVerificacionIdentificacion' },
      { 'Valor': 'tipoIdentificacion', 'Descripcion': 'tipoIdentificacion' },
      { 'Valor': 'razonSocial', 'Descripcion': 'razonSocial' },
      { 'Valor': 'tipoPersona', 'Descripcion': 'tipoPersona' },
      { 'Valor': 'correo', 'Descripcion': 'correo' },
      { 'Valor': 'pais', 'Descripcion': 'pais' },
      { 'Valor': 'ciudad', 'Descripcion': 'ciudad' },
      { 'Valor': 'departamento', 'Descripcion': 'departamento' },
      { 'Valor': 'barrio', 'Descripcion': 'barrio' },
      { 'Valor': 'direccion', 'Descripcion': 'direccion' },
      { 'Valor': 'primerNombre', 'Descripcion': 'primerNombre' },
      { 'Valor': 'segundoNombre', 'Descripcion': 'segundoNombre' },
      { 'Valor': 'apellidos', 'Descripcion': 'apellidos' },
      { 'Valor': 'placa', 'Descripcion': 'placa' },
      { 'Valor': 'modelo', 'Descripcion': 'modelo' },
      { 'Valor': 'informacionAdicional', 'Descripcion': 'informacionAdicional' }
    ];
    this.listNameRuler = json;
  }

  public saveRuler () {
    this.loading = true;
    const json = this.loadJsonRuler();
    this.conectorApiHelper.ResolverPeticionPost(environment.InsertRuler, json).then(response => {
      const body = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      if (body) {
        this.toasterService.pop('success', 'Felicidades ¡¡¡', 'La regla fue creada correctamente');
        this.rulerOne = getRulerModelInstance();
        this.DisableSubmitButtonRulerForm();
        this.showNewRuler = false;
      }
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuesta(error);
      this.toasterService.pop('error', errorMessagge);
    });
  }


  public loadJsonRuler() {
    const jsonRuler = {
      Nombre : this.rulerOne.Name,
      Valor : this.rulerOne.Status
    };
    return jsonRuler;
  }

  public ProcessJson() {
    this.loading = true;
    this.conectorApiHelper.ResolverPeticionPostJson(environment.InsertJson, this.jsonText).then(response => {
      const body = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      if (body) {
        this.toasterService.pop('success', 'Felicidades ¡¡¡', 'El json fué procesado y almacenado correctamente');
        this.jsonText = '';
        this.DisableSubmitButtonTransformerJsonForm();
      }
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuesta(error);
      this.toasterService.pop('error', errorMessagge);
    });
  }

  public deleteRulers() {
    this.loading = true;
    this.conectorApiHelper.ResolverPeticionPut(environment.DeleteRulers).then(response => {
      const body = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      if (body) {
        this.toasterService.pop('success', 'Felicidades ¡¡¡', 'Las reglas del sistema fueron eliminadas correctamente');
        this.jsonText = '';
        this.DisableSubmitButtonTransformerJsonForm();
      }
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuesta(error);
      this.toasterService.pop('error', errorMessagge);
    });
  }

  public DisableSubmitButtonTransformerJsonForm() {
    return this.TransformerJsonForm.invalid;
  }

  public DisableSubmitButtonRulerForm() {
    return this.RulerForm.invalid;
  }

}
