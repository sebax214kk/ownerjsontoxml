import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app-routing';
import { AppComponent } from './app.component';
import { NgxLoadingModule } from 'ngx-loading';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConectorApiHelper } from './Core/Helper/conectroApiHelper';
import { HttpModule } from '@angular/http';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { TranformerJsonComponent } from './tranformer-json/tranformer-json.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OwnerRecordsComponent } from './owner-records/owner-records.component';


@NgModule({
  declarations: [
    AppComponent,
    TranformerJsonComponent,
    OwnerRecordsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    ToasterModule.forRoot(),

  ],
  providers: [appRoutingProviders, ConectorApiHelper],
  bootstrap: [AppComponent]
})
export class AppModule { }
