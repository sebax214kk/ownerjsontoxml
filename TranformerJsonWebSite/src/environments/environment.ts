declare var data: any;

export const environment = {
  production: false,
  rowCountPerPage: '5',
  InsertJson: data.basePathWebApi + 'api/Owner/InsertOwner',
  InsertRuler: data.basePathWebApi + 'api/Ruler/InsertRuler',
  DeleteRulers: data.basePathWebApi + 'api/Ruler/DeleteRulers',
  GetAllOwners: data.basePathWebApi + 'api/Owner/GetOwnerAll/rowCountPerPage/[rowCountPerPage]/pageIndex/[pageIndex]',
  SendEmail: data.basePathWebApi + 'api/Notification/SendEmail/identificacion/'
};

