﻿namespace JsonTransformer.Service.DependencyResolution.ApplicationInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTransformer.Application.Operator.Application;
    using JsonTransformer.Application.Operator.Helper;

    public class OperatorInstaller : IWindsorInstaller
    {
        public string ConnectionString { get; set; }

        public OperatorInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                     Component.For<IJsonApplication>().ImplementedBy<JsonApplication>().Named("JsonApplication")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                      Component.For<IJsonHelper>().ImplementedBy<JsonHelper>().Named("JsonHelper")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                        Component.For<IRulerApplication>().ImplementedBy<RulerApplication>().Named("RulerApplication")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                         Component.For<IRulerHelper>().ImplementedBy<RulerHelper>().Named("RulerHelper")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                             Component.For<IMailHelper>().ImplementedBy<MailHelper>().Named("MailHelper")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                        Component.For<IOwnerApplication>().ImplementedBy<OwnerApplication>().Named("OwnerApplication")
                    .LifeStyle.HybridPerWebRequestPerThread()
                );
        }
    }
}
