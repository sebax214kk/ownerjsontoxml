﻿namespace JsonTransformer.Service.DependencyResolution.ApplicationInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    public class ValidatorInstaller : IWindsorInstaller
    {
        public ValidatorInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(  );
        }

    }
}
