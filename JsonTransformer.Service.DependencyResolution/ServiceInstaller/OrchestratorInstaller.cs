﻿namespace JsonTransformer.Service.DependencyResolution.ServiceInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using JsonTranformer.Domain.Interfaces.ServiceInterfaces;
    using JsonTransformer.Service.Orchestrator.Service;

    public class OrchestratorInstaller : IWindsorInstaller
    {
        public OrchestratorInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                 Component.For<IJsonService>().ImplementedBy<JsonService>().Named("JsonService")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                 Component.For<IRuleService>().ImplementedBy<RuleService>().Named("RuleService")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                 Component.For<INotificationService>().ImplementedBy<NotificationService>().Named("NotificationService")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                 Component.For<IOwnerService>().ImplementedBy<OwnerService>().Named("OwnerService")
                    .LifeStyle.HybridPerWebRequestPerThread()
                );
        }
    }
}
