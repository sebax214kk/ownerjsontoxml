﻿namespace JsonTransformer.Service.DependencyResolution.InfraestructureInstaller
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;
    using JsonTransformer.Domain.Interfaces;
    using JsonTransformer.Domain.Interfaces.InfraestructureInterfaces;
    using JsonTransformer.Infraestructure.DataAccess.Dao;
    using JsonTransformer.Infraestructure.DataAccess.DataAccess;

    public class DataAccessInstaller : IWindsorInstaller
    {
        public string ConnectionString { get; set; }

        public DataAccessInstaller(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IDataAccessObject>().ImplementedBy<DataAccessObject>().Named("Dao")
                    .DependsOn(Dependency.OnValue("ConnectionString", this.ConnectionString))
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IOwnerDac>().ImplementedBy<OwnerDac>().Named("OwnerDac")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<ILocationDac>().ImplementedBy<LocationDac>().Named("LocationDac")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IPersonDac>().ImplementedBy<PersonDac>().Named("PersonDac")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IVehicleDac>().ImplementedBy<VehicleDac>().Named("VehicleDac")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IRulerDac>().ImplementedBy<RulerDac>().Named("RulerDac")
                    .LifeStyle.HybridPerWebRequestPerThread()
                );
        }
    }
}
