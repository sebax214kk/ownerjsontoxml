
IF NOT EXISTS (SELECT * FROM sys.objects WHERE name Like 'PropietarioPersona')
BEGIN
	CREATE TABLE PropietarioPersona(
		PropietarioPersonaId BIGINT IDENTITY PRIMARY KEY,
		PropietarioId BIGINT NOT NULL,
		PrimerNombre VARCHAR(255) NOT NULL,
		SegundoNombre VARCHAR(255) NOT NULL,
		Apellidos VARCHAR(255) NOT NULL,
		CONSTRAINT FK_Persona_Propietario FOREIGN KEY (PropietarioId) REFERENCES dbo.[Propietario](PropietarioId)
	);
END
GO
