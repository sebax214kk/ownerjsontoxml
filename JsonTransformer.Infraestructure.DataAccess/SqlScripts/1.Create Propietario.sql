
IF NOT EXISTS (SELECT * FROM sys.objects WHERE name Like 'Propietario')
BEGIN
	CREATE TABLE Propietario(
		PropietarioId BIGINT IDENTITY PRIMARY KEY,
		Identificacion VARCHAR(20) NOT NULL,
		DigitoVerificacionIdentificacion INT NOT NULL,
		TipoIdentificacion INT NOT NULL,
		RazonSocial VARCHAR(MAX) NOT NULL,
		TipoPersona INT NOT NULL,
		Correo VARCHAR(255) NOT NULL
	);
END
GO
