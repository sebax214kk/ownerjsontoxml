
IF NOT EXISTS (SELECT * FROM sys.objects WHERE name Like 'PropietarioUbicacion')
BEGIN
	CREATE TABLE PropietarioUbicacion(
		PropietarioUbicacionId BIGINT IDENTITY PRIMARY KEY,
		PropietarioId BIGINT NOT NULL,
		pais VARCHAR(255) NOT NULL,
		Ciudad VARCHAR(255) NOT NULL,
		Departamento VARCHAR(255) NOT NULL,
		Barrio VARCHAR(255) NOT NULL,
		Direccion VARCHAR(255) NOT NULL,
		CONSTRAINT FK_Ubicacion_Propietario FOREIGN KEY (PropietarioId) REFERENCES dbo.[Propietario](PropietarioId)
	);
END
GO