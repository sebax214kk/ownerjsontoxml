USE [master]
GO
/****** Object:  Database [JsonTransformer]    Script Date: 19/03/2019 11:45:18 a. m. ******/
CREATE DATABASE [JsonTransformer]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'JsonTransformer', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS2012\MSSQL\DATA\JsonTransformer.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'JsonTransformer_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS2012\MSSQL\DATA\JsonTransformer_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [JsonTransformer] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [JsonTransformer].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [JsonTransformer] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [JsonTransformer] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [JsonTransformer] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [JsonTransformer] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [JsonTransformer] SET ARITHABORT OFF 
GO
ALTER DATABASE [JsonTransformer] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [JsonTransformer] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [JsonTransformer] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [JsonTransformer] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [JsonTransformer] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [JsonTransformer] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [JsonTransformer] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [JsonTransformer] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [JsonTransformer] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [JsonTransformer] SET  DISABLE_BROKER 
GO
ALTER DATABASE [JsonTransformer] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [JsonTransformer] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [JsonTransformer] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [JsonTransformer] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [JsonTransformer] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [JsonTransformer] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [JsonTransformer] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [JsonTransformer] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [JsonTransformer] SET  MULTI_USER 
GO
ALTER DATABASE [JsonTransformer] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [JsonTransformer] SET DB_CHAINING OFF 
GO
ALTER DATABASE [JsonTransformer] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [JsonTransformer] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [JsonTransformer]
GO
/****** Object:  Table [dbo].[Propietario]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Propietario](
	[PropietarioId] [bigint] IDENTITY(1,1) NOT NULL,
	[Identificacion] [varchar](20) NOT NULL,
	[DigitoVerificacionIdentificacion] [int] NOT NULL,
	[TipoIdentificacion] [int] NOT NULL,
	[RazonSocial] [varchar](max) NOT NULL,
	[TipoPersona] [int] NOT NULL,
	[Correo] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PropietarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PropietarioPersona]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropietarioPersona](
	[PropietarioPersonaId] [bigint] IDENTITY(1,1) NOT NULL,
	[PropietarioId] [bigint] NOT NULL,
	[PrimerNombre] [varchar](255) NOT NULL,
	[SegundoNombre] [varchar](255) NOT NULL,
	[Apellidos] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PropietarioPersonaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PropietarioUbicacion]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropietarioUbicacion](
	[PropietarioUbicacionId] [bigint] IDENTITY(1,1) NOT NULL,
	[PropietarioId] [bigint] NOT NULL,
	[pais] [varchar](255) NOT NULL,
	[Ciudad] [varchar](255) NOT NULL,
	[Departamento] [varchar](255) NOT NULL,
	[Barrio] [varchar](255) NOT NULL,
	[Direccion] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PropietarioUbicacionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PropietarioVehiculo]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PropietarioVehiculo](
	[PropietarioVehiculoId] [bigint] IDENTITY(1,1) NOT NULL,
	[PropietarioId] [bigint] NOT NULL,
	[placa] [varchar](7) NOT NULL,
	[Modelo] [int] NOT NULL,
	[InformacionAdicional] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[PropietarioVehiculoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Regla]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Regla](
	[ReglaId] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NOT NULL,
	[Valor] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ReglaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PropietarioPersona]  WITH CHECK ADD  CONSTRAINT [FK_Persona_Propietario] FOREIGN KEY([PropietarioId])
REFERENCES [dbo].[Propietario] ([PropietarioId])
GO
ALTER TABLE [dbo].[PropietarioPersona] CHECK CONSTRAINT [FK_Persona_Propietario]
GO
ALTER TABLE [dbo].[PropietarioUbicacion]  WITH CHECK ADD  CONSTRAINT [FK_Ubicacion_Propietario] FOREIGN KEY([PropietarioId])
REFERENCES [dbo].[Propietario] ([PropietarioId])
GO
ALTER TABLE [dbo].[PropietarioUbicacion] CHECK CONSTRAINT [FK_Ubicacion_Propietario]
GO
ALTER TABLE [dbo].[PropietarioVehiculo]  WITH CHECK ADD  CONSTRAINT [FK_Vehiculo_Propietario] FOREIGN KEY([PropietarioId])
REFERENCES [dbo].[Propietario] ([PropietarioId])
GO
ALTER TABLE [dbo].[PropietarioVehiculo] CHECK CONSTRAINT [FK_Vehiculo_Propietario]
GO
/****** Object:  StoredProcedure [dbo].[EliminarReglas]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[EliminarReglas] 

AS
BEGIN

	DELETE FROM Regla;

END

GO
/****** Object:  StoredProcedure [dbo].[GetAllRulers]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllRulers] 

AS
BEGIN
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;

	SELECT [ReglaId]
		  ,[Nombre]
		  ,[Valor]
	  FROM [dbo].[Regla]
	  where Valor = 1;
		
END

	

GO
/****** Object:  StoredProcedure [dbo].[GetDataOwners]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDataOwners] 
	@Identificacion VARCHAR(20) = NULL,
	@RowCountPerPage INT,
	@PageIndex INT

AS
BEGIN
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
    SET DATEFORMAT DMY;

	WITH TempResult AS(
		SELECT 
			 pro.PropietarioId
			,pro.Identificacion
			,pro.DigitoVerificacionIdentificacion
			,pro.TipoIdentificacion
			,pro.RazonSocial
			,pro.TipoPersona
			,pro.Correo
			,per.PrimerNombre
			,per.SegundoNombre
			,per.Apellidos
			,ubi.pais
			,ubi.Ciudad
			,ubi.Departamento
			,ubi.Barrio
			,ubi.Direccion			
		FROM  Propietario pro (nolock)
		inner join PropietarioPersona per (nolock) on pro.PropietarioId = per.PropietarioId
		inner join PropietarioUbicacion ubi (nolock) on ubi.PropietarioId = per.PropietarioId
		WHERE @Identificacion IS NULL OR @Identificacion = pro.Identificacion
		), TempCount AS (SELECT COUNT(*) AS TotalCount FROM TempResult)

	SELECT * FROM TempResult, TempCount
		  ORDER BY TempResult.[Identificacion] DESC
        OFFSET(@PageIndex - 1) * @RowCountPerPage ROWS FETCH NEXT @RowCountPerPage ROWS ONLY;
END


	

GO
/****** Object:  StoredProcedure [dbo].[InsertarPropietario]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertarPropietario] 

	@Identificacion varchar(20) = NULL,
	@DigitoVerificacionIdentificacion int = NULL,
	@TipoIdentificacion int = NULL,
	@RazonSocial varchar(max) = NULL,
	@TipoPersona int = NULL,
	@Correo varchar(255)= NULL
AS
BEGIN

	INSERT INTO [dbo].[Propietario]
			   ([Identificacion]
			   ,[DigitoVerificacionIdentificacion]
			   ,[TipoIdentificacion]
			   ,[RazonSocial]
			   ,[TipoPersona]
			   ,[Correo])
		 VALUES
			   (@Identificacion ,
				@DigitoVerificacionIdentificacion,
				@TipoIdentificacion,
				@RazonSocial,
				@TipoPersona,
				@Correo)


	SELECT * 
	FROM Propietario
	WHERE PropietarioId = SCOPE_IDENTITY();
END

GO
/****** Object:  StoredProcedure [dbo].[InsertarPropietarioPersona]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertarPropietarioPersona] 

	@PropietarioId bigint = NULL,
	@PrimerNombre varchar(255)= NULL,
	@SegundoNombre varchar(255)= NULL,
	@Apellidos varchar(255)= NULL

AS
BEGIN

INSERT INTO [dbo].[PropietarioPersona]
           ([PropietarioId]
           ,[PrimerNombre]
           ,[SegundoNombre]
           ,[Apellidos])
     VALUES
           (@PropietarioId,
			@PrimerNombre,
			@SegundoNombre,
			@Apellidos)


	SELECT * 
	FROM [dbo].[PropietarioPersona]
	WHERE PropietarioPersonaId = SCOPE_IDENTITY();
END

GO
/****** Object:  StoredProcedure [dbo].[InsertarPropietarioUbicacion]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertarPropietarioUbicacion] 

	@PropietarioId bigint = NULL,
	@pais varchar(255) = NULL,
	@Ciudad varchar(255) = NULL,
	@Departamento varchar(255) = NULL,
	@Barrio varchar(255) = NULL,
	@Direccion varchar(255) = NULL
AS
BEGIN

	INSERT INTO [dbo].[PropietarioUbicacion]
			   ([PropietarioId]
			   ,[pais]
			   ,[Ciudad]
			   ,[Departamento]
			   ,[Barrio]
			   ,[Direccion])
		 VALUES
			   (@PropietarioId,
				@pais,
				@Ciudad,
				@Departamento,
				@Barrio ,
				@Direccion)

	SELECT * 
	FROM [dbo].[PropietarioUbicacion]
	WHERE PropietarioUbicacionId = SCOPE_IDENTITY();
END

GO
/****** Object:  StoredProcedure [dbo].[InsertarPropietarioVehiculo]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertarPropietarioVehiculo] 

	@PropietarioId bigint = NULL,
	@Placa varchar(7)= NULL,
	@Modelo int= NULL,
	@InformacionAdicional varchar(max)= NULL

AS
BEGIN


INSERT INTO [dbo].[PropietarioVehiculo]
           ([PropietarioId]
           ,[placa]
           ,[Modelo]
           ,[InformacionAdicional])
     VALUES
			(@PropietarioId,
			@Placa,
			@Modelo,
			@InformacionAdicional)

	SELECT * 
	FROM [dbo].[PropietarioVehiculo]
	WHERE PropietarioVehiculoId = SCOPE_IDENTITY();
END

GO
/****** Object:  StoredProcedure [dbo].[InsertarRegla]    Script Date: 19/03/2019 11:45:19 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertarRegla] 

	@Nombre varchar(255) = NULL,
	@Valor bit = NULL

AS
BEGIN


INSERT INTO [dbo].[Regla]
           ([Nombre],
           [Valor])
     VALUES
           (@Nombre,
            @Valor)

END

GO
USE [master]
GO
ALTER DATABASE [JsonTransformer] SET  READ_WRITE 
GO
