
IF NOT EXISTS (SELECT * FROM sys.objects WHERE name Like 'PropietarioVehiculo')
BEGIN
	CREATE TABLE PropietarioVehiculo(
		PropietarioVehiculoId BIGINT IDENTITY PRIMARY KEY,
		PropietarioId BIGINT NOT NULL,
		placa VARCHAR(7) NOT NULL,
		Modelo INT NOT NULL,
		InformacionAdicional VARCHAR(MAX),
		CONSTRAINT FK_Vehiculo_Propietario FOREIGN KEY (PropietarioId) REFERENCES dbo.[Propietario](PropietarioId)
	);
END
GO




