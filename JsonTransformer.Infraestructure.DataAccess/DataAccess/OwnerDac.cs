﻿namespace JsonTransformer.Infraestructure.DataAccess.DataAccess
{
    using System.Data;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;
    using JsonTransformer.Domain.Entities.Models;
    using JsonTransformer.Domain.Interfaces.InfraestructureInterfaces;

    public class OwnerDac : IOwnerDac
    {
        public IDataAccessObject Dao { get; set; }

        public OwnerDac(IDataAccessObject Dao)
        {
            this.Dao = Dao;
        }

        public DataSet InsertOwner(OwnerModel ownerModel)
        {
            var response = Dao.SqlQueryResult("InsertarPropietario",
               new ModelDao("Identificacion", SqlDbType.VarChar, ownerModel.identificacion),
               new ModelDao("DigitoVerificacionIdentificacion", SqlDbType.Int, ownerModel.digitoVerificacionIdentificacion),
               new ModelDao("TipoIdentificacion", SqlDbType.Int, ownerModel.tipoIdentificacion),
               new ModelDao("RazonSocial", SqlDbType.VarChar, ownerModel.razonSocial),
               new ModelDao("TipoPersona", SqlDbType.Int, ownerModel.tipoPersona),
               new ModelDao("Correo", SqlDbType.VarChar, ownerModel.correo));

            return response;
        }

        public DataSet GetOwnerAll(string identification, string rowCountPerPage, string pageIndex)
        {
            var response = Dao.SqlQueryResult("GetDataOwners",
                new ModelDao("Identificacion", SqlDbType.VarChar, identification),
                new ModelDao("RowCountPerPage", SqlDbType.Int, rowCountPerPage),
                new ModelDao("PageIndex", SqlDbType.Int, pageIndex));

            return response;
        }
    }
}
