﻿namespace JsonTransformer.Infraestructure.DataAccess.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;
    using JsonTransformer.Domain.Entities.Models;
    using JsonTransformer.Domain.Interfaces.InfraestructureInterfaces;

    public class VehicleDac : IVehicleDac
    {
        public IDataAccessObject Dao { get; set; }

        public VehicleDac(IDataAccessObject Dao)
        {
            this.Dao = Dao;
        }

        public void Insertvehicle(VehicleModel vehicleModel)
        {
            Dao.SqlUpdate("InsertarPropietarioVehiculo",
             new ModelDao("PropietarioId", SqlDbType.Int, vehicleModel.propietarioId),
             new ModelDao("Placa", SqlDbType.VarChar, vehicleModel.placa),
             new ModelDao("Modelo", SqlDbType.Int, vehicleModel.modelo),
             new ModelDao("InformacionAdicional", SqlDbType.VarChar, vehicleModel.informaqcionAdicionalJson));
        }
    }
}
