﻿namespace JsonTransformer.Infraestructure.DataAccess.DataAccess
{
    using System.Data;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;
    using JsonTransformer.Domain.Entities.Models;
    using JsonTransformer.Domain.Interfaces.InfraestructureInterfaces;

    public class RulerDac : IRulerDac
    {
        public IDataAccessObject Dao { get; set; }

        public RulerDac(IDataAccessObject Dao)
        {
            this.Dao = Dao;
        }

        public bool InsertRule(RulerModel rulerModel)
        {
            var response = Dao.SqlUpdate("InsertarRegla",
             new ModelDao("Nombre", SqlDbType.VarChar, rulerModel.Nombre),
             new ModelDao("Valor", SqlDbType.Bit, rulerModel.Valor));

            return response;
        }

        public bool DeleteRules()
        {
            var response = Dao.SqlUpdate("EliminarReglas");

            return response;
        }

        public DataSet GetAllRulers()
        {
            var response = Dao.SqlQueryResult("GetAllRulers");

            return response;
        }
    }
}
