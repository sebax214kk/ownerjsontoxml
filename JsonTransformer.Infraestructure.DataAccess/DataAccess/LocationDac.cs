﻿namespace JsonTransformer.Infraestructure.DataAccess.DataAccess
{
    using System.Data;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;
    using JsonTransformer.Domain.Entities.Models;
    using JsonTransformer.Domain.Interfaces.InfraestructureInterfaces;

    public class LocationDac : ILocationDac
    {
        public IDataAccessObject Dao { get; set; }

        public LocationDac(IDataAccessObject Dao)
        {
            this.Dao = Dao;
        }

        public void InsertLocation(LocationModel locationModel)
        {
            Dao.SqlUpdate("InsertarPropietarioUbicacion",
             new ModelDao("PropietarioId", SqlDbType.Int, locationModel.propietarioId),
             new ModelDao("pais", SqlDbType.VarChar, locationModel.pais),
             new ModelDao("Ciudad", SqlDbType.VarChar, locationModel.ciudad),
             new ModelDao("Departamento", SqlDbType.VarChar, locationModel.departamento),
             new ModelDao("Barrio", SqlDbType.VarChar, locationModel.barrio),
             new ModelDao("Direccion", SqlDbType.VarChar, locationModel.direccion));
        }
    }
}
