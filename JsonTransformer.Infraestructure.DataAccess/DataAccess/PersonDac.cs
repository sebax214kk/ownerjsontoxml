﻿namespace JsonTransformer.Infraestructure.DataAccess.DataAccess
{
    using System.Data;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;
    using JsonTransformer.Domain.Entities.Models;
    using JsonTransformer.Domain.Interfaces.InfraestructureInterfaces;

    public class PersonDac : IPersonDac
    {
        public IDataAccessObject Dao { get; set; }

        public PersonDac(IDataAccessObject Dao)
        {
            this.Dao = Dao;
        }

        public void InserPersona(PersonModel personModel)
        {
            Dao.SqlUpdate("InsertarPropietarioPersona",
             new ModelDao("PropietarioId", SqlDbType.Int, personModel.propietarioId),
             new ModelDao("PrimerNombre", SqlDbType.VarChar, personModel.primerNombre),
             new ModelDao("SegundoNombre", SqlDbType.VarChar, personModel.segundoNombre),
             new ModelDao("Apellidos", SqlDbType.VarChar, personModel.apellidos));
        }
    }
}
