SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertarPropietario]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InsertarPropietario] AS' 
END
GO
ALTER PROCEDURE [dbo].[InsertarPropietario] 

	@Identificacion varchar(20) = NULL,
	@DigitoVerificacionIdentificacion int = NULL,
	@TipoIdentificacion int = NULL,
	@RazonSocial varchar(max) = NULL,
	@TipoPersona int = NULL,
	@Correo varchar(255)= NULL
AS
BEGIN

	INSERT INTO [dbo].[Propietario]
			   ([Identificacion]
			   ,[DigitoVerificacionIdentificacion]
			   ,[TipoIdentificacion]
			   ,[RazonSocial]
			   ,[TipoPersona]
			   ,[Correo])
		 VALUES
			   (@Identificacion ,
				@DigitoVerificacionIdentificacion,
				@TipoIdentificacion,
				@RazonSocial,
				@TipoPersona,
				@Correo)


	SELECT * 
	FROM Propietario
	WHERE PropietarioId = SCOPE_IDENTITY();
END
GO



