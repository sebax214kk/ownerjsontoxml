SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertarPropietarioUbicacion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InsertarPropietarioUbicacion] AS' 
END
GO
ALTER PROCEDURE [dbo].[InsertarPropietarioUbicacion] 

	@PropietarioId bigint = NULL,
	@pais varchar(255) = NULL,
	@Ciudad varchar(255) = NULL,
	@Departamento varchar(255) = NULL,
	@Barrio varchar(255) = NULL,
	@Direccion varchar(255) = NULL
AS
BEGIN

	INSERT INTO [dbo].[PropietarioUbicacion]
			   ([PropietarioId]
			   ,[pais]
			   ,[Ciudad]
			   ,[Departamento]
			   ,[Barrio]
			   ,[Direccion])
		 VALUES
			   (@PropietarioId,
				@pais,
				@Ciudad,
				@Departamento,
				@Barrio ,
				@Direccion)

	SELECT * 
	FROM [dbo].[PropietarioUbicacion]
	WHERE PropietarioUbicacionId = SCOPE_IDENTITY();
END
GO
