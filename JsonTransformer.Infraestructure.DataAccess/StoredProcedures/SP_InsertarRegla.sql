SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertarRegla]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InsertarRegla] AS' 
END
GO
ALTER PROCEDURE [dbo].[InsertarRegla] 

	@Nombre varchar(255) = NULL,
	@Valor bit = NULL

AS
BEGIN


INSERT INTO [dbo].[Regla]
           ([Nombre],
           [Valor])
     VALUES
           (@Nombre,
            @Valor)

END
GO
