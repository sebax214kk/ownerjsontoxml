SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDataOwnersPivot]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetDataOwnersPivot] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetDataOwnersPivot] 

AS
BEGIN
			 SELECT	 PropietarioId
			,placa
			,Modelo
			,InformacionAdicional
			,[1] as Vehiculo1
			,[2] as Vehiculo2	
			
	FROM(
		SELECT	pro.PropietarioId
				,veh.PropietarioVehiculoId
				,veh.placa
				,veh.Modelo
				,veh.InformacionAdicional
		FROM  Propietario pro (nolock)
			inner join PropietarioPersona per (nolock) on pro.PropietarioId = per.PropietarioId
			inner join PropietarioVehiculo veh (nolock) on veh.PropietarioId = per.PropietarioId) as p
		PIVOT (COUNT (PropietarioVehiculoId)
			FOR PropietarioVehiculoId IN
				( [1], [2])
		) AS pvt
END
