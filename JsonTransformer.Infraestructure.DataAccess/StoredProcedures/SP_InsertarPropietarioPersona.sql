SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertarPropietarioPersona]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InsertarPropietarioPersona] AS' 
END
GO
ALTER PROCEDURE [dbo].[InsertarPropietarioPersona] 

	@PropietarioId bigint = NULL,
	@PrimerNombre varchar(255)= NULL,
	@SegundoNombre varchar(255)= NULL,
	@Apellidos varchar(255)= NULL

AS
BEGIN

INSERT INTO [dbo].[PropietarioPersona]
           ([PropietarioId]
           ,[PrimerNombre]
           ,[SegundoNombre]
           ,[Apellidos])
     VALUES
           (@PropietarioId,
			@PrimerNombre,
			@SegundoNombre,
			@Apellidos)


	SELECT * 
	FROM [dbo].[PropietarioPersona]
	WHERE PropietarioPersonaId = SCOPE_IDENTITY();
END
GO
