SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDataOwners]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetDataOwners] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetDataOwners] 
	@Identificacion VARCHAR(20) = NULL,
	@RowCountPerPage INT,
	@PageIndex INT

AS
BEGIN
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;
    SET DATEFORMAT DMY;

	WITH TempResult AS(
		SELECT 
			 pro.PropietarioId
			,pro.Identificacion
			,pro.DigitoVerificacionIdentificacion
			,pro.TipoIdentificacion
			,pro.RazonSocial
			,pro.TipoPersona
			,pro.Correo
			,per.PrimerNombre
			,per.SegundoNombre
			,per.Apellidos
			,ubi.pais
			,ubi.Ciudad
			,ubi.Departamento
			,ubi.Barrio
			,ubi.Direccion			
		FROM  Propietario pro (nolock)
		inner join PropietarioPersona per (nolock) on pro.PropietarioId = per.PropietarioId
		inner join PropietarioUbicacion ubi (nolock) on ubi.PropietarioId = per.PropietarioId
		WHERE @Identificacion IS NULL OR @Identificacion = pro.Identificacion
		), TempCount AS (SELECT COUNT(*) AS TotalCount FROM TempResult)

	SELECT * FROM TempResult, TempCount
		  ORDER BY TempResult.[Identificacion] DESC
        OFFSET(@PageIndex - 1) * @RowCountPerPage ROWS FETCH NEXT @RowCountPerPage ROWS ONLY;
END


	
