SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertarPropietarioVehiculo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InsertarPropietarioVehiculo] AS' 
END
GO
ALTER PROCEDURE [dbo].[InsertarPropietarioVehiculo] 

	@PropietarioId bigint = NULL,
	@Placa varchar(7)= NULL,
	@Modelo int= NULL,
	@InformacionAdicional varchar(max)= NULL

AS
BEGIN


INSERT INTO [dbo].[PropietarioVehiculo]
           ([PropietarioId]
           ,[placa]
           ,[Modelo]
           ,[InformacionAdicional])
     VALUES
			(@PropietarioId,
			@Placa,
			@Modelo,
			@InformacionAdicional)

	SELECT * 
	FROM [dbo].[PropietarioVehiculo]
	WHERE PropietarioVehiculoId = SCOPE_IDENTITY();
END
GO
