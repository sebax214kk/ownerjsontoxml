SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAllRulers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAllRulers] AS' 
END
GO
ALTER PROCEDURE [dbo].[GetAllRulers] 

AS
BEGIN
	SET NOCOUNT ON;
	SET QUOTED_IDENTIFIER ON;

	SELECT [ReglaId]
		  ,[Nombre]
		  ,[Valor]
	  FROM [dbo].[Regla]
	  where Valor = 1;
		
END

	
