﻿namespace JsonTransformer.Application.Operator.Application
{
    using System.Data;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;

    public class RulerApplication : IRulerApplication
    {
        public IRulerDac RulerDac { get; set; }

        public RulerApplication(IRulerDac rulerDac)
        {
            this.RulerDac = rulerDac;
        }

        public bool InsertRule(RulerModel rulerModel)
        {
            return RulerDac.InsertRule(rulerModel);
        }

        public bool DeleteRules()
        {
            return RulerDac.DeleteRules();
        }

        public DataSet GetAllRules()
        {
            return RulerDac.GetAllRulers();
        }
    }
}
