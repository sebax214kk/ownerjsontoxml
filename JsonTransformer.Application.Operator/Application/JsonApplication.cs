﻿namespace JsonTransformer.Application.Operator.Application
{
    using System;
    using System.Data;
    using System.Transactions;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;


    public class JsonApplication : IJsonApplication
    {

        public IOwnerDac OwnerDac { get; set; }
        public ILocationDac LocationDac { get; set; }
        public IPersonDac PersonDac { get; set; }
        public IVehicleDac VehicleDac { get; set; }

        public JsonApplication(IOwnerDac ownerDac, ILocationDac locationDac, IPersonDac personDac, IVehicleDac vehicleDac)
        {
            this.OwnerDac = ownerDac;
            this.LocationDac = locationDac;
            this.PersonDac = personDac;
            this.VehicleDac = vehicleDac;
        }

        public bool InsertJson(JsonStructureModel jsonStructureModel)
        {
            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                int idOwner;

                DataSet dataSet = OwnerDac.InsertOwner(jsonStructureModel.Propietario);

                if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
                {
                    idOwner = Convert.ToInt32(dataSet.Tables[0].Rows[0]["PropietarioId"]);
                    jsonStructureModel.Propietario.ubicacion.propietarioId = idOwner;
                    jsonStructureModel.Propietario.persona.propietarioId = idOwner;


                    LocationDac.InsertLocation(jsonStructureModel.Propietario.ubicacion);

                    PersonDac.InserPersona(jsonStructureModel.Propietario.persona);

                    foreach (var vehicle in jsonStructureModel.Propietario.vehiculos)
                    {
                        vehicle.propietarioId = idOwner;
                        vehicle.informaqcionAdicionalJson = Newtonsoft.Json.JsonConvert.SerializeObject(vehicle.informacionAdicional);
                        VehicleDac.Insertvehicle(vehicle);
                    }
                }

                transaction.Complete();
            }

            return true;
        }
    }
}
