﻿namespace JsonTransformer.Application.Operator.Application
{
    using System.Collections.Generic;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;

    public class OwnerApplication : IOwnerApplication
    {
        public IOwnerDac OwnerDac { get; set; }
        public IMailHelper MailHelper { get; set; }

        public OwnerApplication (IOwnerDac ownerDac, IMailHelper mailHelper) {
            this.OwnerDac = ownerDac;
            this.MailHelper = mailHelper;
        }

        public List<OwnerModel> GetOwnerAll(string rowCountPerPage, string pageIndex)
        {
            return  this.MailHelper.TransformerToModel(OwnerDac.GetOwnerAll(null, rowCountPerPage, pageIndex));
        }

        public OwnerModel GetOwnerByIdentification(string identification)
        {
            return this.MailHelper.GetOwner(OwnerDac.GetOwnerAll(identification, "1", "1"));

        }
    }
}
