﻿namespace JsonTransformer.Application.Operator.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTranformer.Domain.Interfaces.InfraestructureInterfaces;
    using JsonTransformer.Domain.Entities.Enum;
    using JsonTransformer.Domain.Entities.Helper;

    public class RulerHelper : IRulerHelper
    {
        IRulerDac RulerDac { get; set; }
        IJsonHelper JsonHelper { get; set; }

        public RulerHelper(IRulerDac rulerDac, IJsonHelper jsonHelper)
        {
            this.RulerDac = rulerDac;
            this.JsonHelper = jsonHelper;
        }
        public RulerModel MapperJsonToModel(string json)
        {
            RulerModel rulerModel = new RulerModel();

            try
            {
                if (!string.IsNullOrEmpty(json))
                {
                    rulerModel = Newtonsoft.Json.JsonConvert.DeserializeObject<RulerModel>(json);
                }

                return rulerModel;
            }
            catch (TransformerException exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new TransformerException("MapperJsonToModelRuler", ErrorMessagesEnum.JsonInvalidRuler, ex);
            }
        }

        public string ValidateRuler(OwnerModel owner)
        {
            var xml = JsonHelper.GetXMLFromObject(owner);

            XElement root = XElement.Parse(xml);

            List<RulerModel> listRuler = GetAllRulers();

            foreach (var ruler in listRuler)
            {
                if (xml.Contains(ruler.Nombre))
                {
                    root.Element(ruler.Nombre).Remove();
                }
            }

            string xmlresult = root.ToString();

            return xmlresult;
        }

        private List<RulerModel> GetAllRulers()
        {
            DataSet dataSet = this.RulerDac.GetAllRulers();

            if (dataSet != null && dataSet.Tables[0].Rows.Count > 0)
            {
                var rulerList = from row in dataSet.Tables[0].AsEnumerable()
                                select new RulerModel()
                                {
                                    Nombre = row["Nombre"].ToString(),
                                    Valor = Convert.ToBoolean(row["Valor"])
                                };

                return rulerList.ToList();
            }

            throw new TransformerException("GetAllRulers", ErrorMessagesEnum.RulerNoFound, null);

        }
    }
}
