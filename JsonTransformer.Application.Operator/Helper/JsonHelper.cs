﻿namespace JsonTransformer.Application.Operator.Helper
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTransformer.Domain.Entities.Enum;
    using JsonTransformer.Domain.Entities.Helper;
    using JsonTransformer.Domain.Entities.Logger;

    public class JsonHelper : IJsonHelper
    {
        public JsonStructureModel MapperJsonStructureModel(string cadena)
        {
            JsonStructureModel jsonStructureModel = new JsonStructureModel();

            try
            {
                if (!string.IsNullOrEmpty(cadena))
                {

                    jsonStructureModel = Newtonsoft.Json.JsonConvert.DeserializeObject<JsonStructureModel>(cadena);
                }

                return jsonStructureModel;
            }
            catch (TransformerException exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                Logger.Current.Error("Error MapperJsonStructureModel ", ex);
                throw new TransformerException("MapperJsonStructureModel", ErrorMessagesEnum.JsonInvalid, ex);
            }

        }

        public string GetXMLFromObject(object objectNew)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(objectNew.GetType());
                StringWriter sw = new StringWriter();
                XmlTextWriter tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, objectNew);
                return sw.ToString();
            }
            catch (TransformerException exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                Logger.Current.Error("Error GetXMLFromObject ", ex);
                throw new TransformerException("GetXMLFromObject", ErrorMessagesEnum.XmlToObjectError, ex);
            }

        }


    }
}
