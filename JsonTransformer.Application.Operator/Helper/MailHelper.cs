﻿namespace JsonTransformer.Application.Operator.Helper
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTransformer.Domain.Entities.Enum;
    using JsonTransformer.Domain.Entities.Helper;
    using JsonTransformer.Domain.Entities.Logger;

    public class MailHelper : IMailHelper
    {
        public void EnviarMensaje(MailConfigurationModel mailConfigurationModel, string recipient, string subject, bool isHtml, string body)
        {
            if (recipient.Trim().Length <= 0)
                throw new ApplicationException("La dirección de correo destino no es válida");

            SmtpClient smtpClient = new SmtpClient(mailConfigurationModel.SmtpHost, mailConfigurationModel.SmtpPort)
            {
                Credentials = new NetworkCredential(mailConfigurationModel.SmtpUserName, mailConfigurationModel.SmtpPassword)
            };

            MailMessage mensaje = ObtenerMensaje(mailConfigurationModel, recipient, mailConfigurationModel.Sender, body, isHtml, subject);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = mailConfigurationModel.EnableSsl;
            smtpClient.Send(mensaje);

        }

        private MailMessage ObtenerMensaje(MailConfigurationModel mailConfigurationModel, string recipient, string from, string textoMensaje, bool esHtml, string titulo)
        {
            if (from.Trim().Length == 0)
                from = mailConfigurationModel.Sender;

            MailMessage mensaje = new MailMessage { From = new MailAddress(from, mailConfigurationModel.SenderName) };

            mensaje.To.Add(recipient);
            mensaje.From = new MailAddress(from, mailConfigurationModel.SenderName);
            mensaje.Sender = new MailAddress(from);
            mensaje.Body = textoMensaje;
            mensaje.IsBodyHtml = esHtml;
            mensaje.Subject = titulo;
            mensaje.Priority = MailPriority.Normal;

            return mensaje;
        }

        public MailConfigurationModel setConfigurationMail()
        {
            MailConfigurationModel mailConfigurationModel = new MailConfigurationModel();

            mailConfigurationModel.SmtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            mailConfigurationModel.SmtpHost = ConfigurationManager.AppSettings["SmtpHost"];
            mailConfigurationModel.SmtpUserName = ConfigurationManager.AppSettings["SmtpUserName"];
            mailConfigurationModel.SmtpPassword = ConfigurationManager.AppSettings["SmtpPassword"];
            mailConfigurationModel.Sender = ConfigurationManager.AppSettings["SmtpUserName"];
            mailConfigurationModel.SenderName = ConfigurationManager.AppSettings["SenderName"];
            mailConfigurationModel.EnableSsl = true;
            mailConfigurationModel.SmtpDeliveryMethod = (int)SmtpDeliveryMethod.Network;

            return mailConfigurationModel;
        }

        public List<OwnerModel> TransformerToModel(DataSet dataset)
        {
            if (dataset != null && dataset.Tables[0].Rows.Count > 0)
            {

                var OwnerModelList = from row in dataset.Tables[0].AsEnumerable()
                                     select new OwnerModel()
                                     {
                                         identificacion = row["Identificacion"].ToString(),
                                         digitoVerificacionIdentificacion = Convert.ToInt32(row["DigitoVerificacionIdentificacion"]),
                                         tipoIdentificacion = Convert.ToInt32(row["TipoIdentificacion"]),
                                         razonSocial = row["RazonSocial"].ToString(),
                                         tipoPersona = Convert.ToInt32(row["TipoPersona"]),
                                         correo = row["Correo"].ToString(),
                                     };

                return OwnerModelList.ToList();
            }

            throw new TransformerException("TransformerToModel", ErrorMessagesEnum.OwnerNoFoundError, null);

        }

        public OwnerModel GetOwner(DataSet dataset)
        {
            try
            {
                if (dataset != null && dataset.Tables[0].Rows.Count > 0)
                {

                    var OwnerModelList = from row in dataset.Tables[0].AsEnumerable()
                                         select new OwnerModel()
                                         {
                                             identificacion = row["Identificacion"].ToString(),
                                             digitoVerificacionIdentificacion = Convert.ToInt32(row["DigitoVerificacionIdentificacion"]),
                                             tipoIdentificacion = Convert.ToInt32(row["TipoIdentificacion"]),
                                             razonSocial = row["RazonSocial"].ToString(),
                                             tipoPersona = Convert.ToInt32(row["TipoPersona"]),
                                             correo = row["Correo"].ToString(),
                                         };

                    return OwnerModelList.FirstOrDefault();
                }

                return null;
            }
            catch (TransformerException exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                Logger.Current.Error("Error GetOwner ", ex);
                throw new TransformerException("GetOwner", ErrorMessagesEnum.OwnerNoFoundError, null);
            }
        }
    }
}

