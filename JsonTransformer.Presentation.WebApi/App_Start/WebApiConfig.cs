﻿namespace JsonTransformer.Presentation.WebApi
{
    using System.Web.Http;
    using System.Web.Http.Cors;
    using JsonTransformer.Presentation.WebApi.FIlters;


    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));

            config.MessageHandlers.Add(new ResponseWrappingHandler());

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
