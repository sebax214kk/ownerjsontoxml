﻿namespace JsonTransformer.Presentation.WebApi.Controllers
{
    using System.Threading.Tasks;
    using System.Web.Http;
    using JsonTranformer.Domain.Interfaces.ServiceInterfaces;

    public class OwnerController : ApiController
    {
        public IJsonService JsonService { get; set; }
        public IOwnerService OwnerService { get; set; }

        public OwnerController(IJsonService jsonService, IOwnerService ownerService)
        {
            this.JsonService = jsonService;
            this.OwnerService = ownerService;
        }

        [HttpPost]
        [Route("api/Owner/InsertOwner")]
        public async Task<IHttpActionResult> InsertData()
        {
            var json = await this.Request.Content.ReadAsStringAsync();

            return Ok(this.JsonService.InsertJsonString(json));
        }

        [HttpGet]
        [Route("api/Owner/GetOwnerAll/rowCountPerPage/{rowCountPerPage}/pageIndex/{pageIndex}")]
        public IHttpActionResult GetAllOwners(string rowCountPerPage, string pageIndex)
        {
            return Ok(this.OwnerService.GetAllOwners(rowCountPerPage, pageIndex));
        }

    }
}
