﻿using System.Threading.Tasks;
using System.Web.Http;
using JsonTranformer.Domain.Interfaces.ServiceInterfaces;

namespace JsonTransformer.Presentation.WebApi.Controllers
{
    public class RuleController : ApiController
    {
        public IRuleService RuleService { get; set; }

        public RuleController(IRuleService ruleService)
        {
            this.RuleService = ruleService;
        }

        [HttpPost]
        [Route("api/Ruler/InsertRuler")]
        public async Task<IHttpActionResult> InsertRuler()
        {
            var json = await this.Request.Content.ReadAsStringAsync();

            return Ok(this.RuleService.InsertRule(json));
        }

        [HttpPut]
        [Route("api/Ruler/DeleteRulers")]
        public IHttpActionResult DeleteRulers()
        {
            return Ok(this.RuleService.DeleteRules());
        }
    }
}
