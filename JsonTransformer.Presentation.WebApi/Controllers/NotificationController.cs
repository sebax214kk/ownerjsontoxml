﻿namespace JsonTransformer.Presentation.WebApi.Controllers
{
    using System.Web.Http;
    using JsonTranformer.Domain.Interfaces.ServiceInterfaces;

    public class NotificationController : ApiController
    {

        public INotificationService NotificationService { get; set; }

        public NotificationController(INotificationService notificationService)
        {
            this.NotificationService = notificationService;
        }

        [HttpGet]
        [Route("api/Notification/SendEmail/identificacion/{identificacion}")]
        public IHttpActionResult SendEmail(string identificacion)
        {
            return Ok(this.NotificationService.SendEmail(identificacion));
        }

    }
}
