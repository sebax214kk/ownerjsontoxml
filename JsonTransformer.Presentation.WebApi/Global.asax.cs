﻿namespace JsonTransformer.Presentation.WebApi
{
    using Castle.MicroKernel.Lifestyle;
    using Castle.MicroKernel.Resolvers.SpecializedResolvers;
    using System.Configuration;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using JsonTransformer.Presentation.WebApi.Windsor;
    using JsonTransformer.Service.DependencyResolution.ApplicationInstaller;
    using JsonTransformer.Service.DependencyResolution.InfraestructureInstaller;
    using JsonTransformer.Service.DependencyResolution.ServiceInstaller;

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializeIoC(System.Web.Http.GlobalConfiguration.Configuration);
        }

        public static void InitializeIoC(HttpConfiguration configuration)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["TransformerJsonConnection"].ToString();

            var windsor = Ragolo.Core.IoC.IocHelper.Instance;
            var contenedor = windsor.GetContainer();

            windsor.Install(new DataAccessInstaller(ConnectionString));

            windsor.Install(new OperatorInstaller());
            windsor.Install(new ValidatorInstaller());

            windsor.Install(new OrchestratorInstaller());

            windsor.Install(new Windsor.Installer.Installer());

            WindsorControllerFactory controllerFactory = new WindsorControllerFactory(contenedor.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            contenedor.Kernel.Resolver.AddSubResolver(new CollectionResolver(contenedor.Kernel, true));
            contenedor.BeginScope();
            var dependencyResolver = new WindsorDependencyResolver(contenedor);
            configuration.DependencyResolver = dependencyResolver;
        }

    }
}
