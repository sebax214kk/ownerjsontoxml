﻿namespace JsonTranformer.Domain.Interfaces.InfraestructureInterfaces
{
    using System.Data;
    using JsonTranformer.Domain.Entities.Models;

    public interface IRulerDac
    {
        bool InsertRule(RulerModel rulerModel);

        bool DeleteRules();

        DataSet GetAllRulers();

    }
}
