﻿namespace JsonTranformer.Domain.Interfaces.InfraestructureInterfaces
{
    using System.Data;
    using JsonTranformer.Domain.Entities.Models;

    public interface IOwnerDac
    {
        /// <summary>
        /// Inserta un nuevo Propietario
        /// </summary>
        /// <param name="ownerModel"></param>
        DataSet InsertOwner(OwnerModel ownerModel);

        /// <summary>
        /// Obtiene todos los propietarios
        /// </summary>
        /// <returns></returns>
        DataSet GetOwnerAll(string identification, string rowCountPerPage, string pageIndex);
    }
}
