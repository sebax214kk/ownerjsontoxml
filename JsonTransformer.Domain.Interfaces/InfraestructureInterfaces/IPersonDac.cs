﻿namespace JsonTranformer.Domain.Interfaces.InfraestructureInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using JsonTranformer.Domain.Entities.Models;

    public interface IPersonDac
    {
        /// <summary>
        /// Inserta una persona relaciona al propietario
        /// </summary>
        /// <param name="personModel"></param>
        void InserPersona(PersonModel personModel);
    }
}
