﻿namespace JsonTranformer.Domain.Interfaces.InfraestructureInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using JsonTranformer.Domain.Entities.Models;

    public interface IVehicleDac
    {
        /// <summary>
        /// Inserta un nuevo vehiculo relacionado aun propietario
        /// </summary>
        /// <param name="vehicleModel"></param>
        void Insertvehicle(VehicleModel vehicleModel);
    }
}
