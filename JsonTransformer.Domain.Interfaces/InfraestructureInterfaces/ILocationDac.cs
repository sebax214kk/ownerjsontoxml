﻿namespace JsonTranformer.Domain.Interfaces.InfraestructureInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using JsonTranformer.Domain.Entities.Models;

    public interface ILocationDac
    {
        /// <summary>
        /// Inserta una nueva ubicacion relacionada a un propietario
        /// </summary>
        /// <param name="locationModel"></param>
        void InsertLocation(LocationModel locationModel);
    }
}
