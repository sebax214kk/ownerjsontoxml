﻿namespace JsonTranformer.Domain.Interfaces.ServiceInterfaces
{
    using System.Collections.Generic;
    using System.Data;
    using JsonTranformer.Domain.Entities.Models;

    public interface IOwnerService
    {
        List<OwnerModel> GetAllOwners(string rowCountPerPage, string pageIndex);
    }
}
