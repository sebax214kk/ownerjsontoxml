﻿namespace JsonTranformer.Domain.Interfaces.ServiceInterfaces
{
    public interface INotificationService
    {
        bool SendEmail( string identification);
    }
}
