﻿using JsonTranformer.Domain.Entities.Models;

namespace JsonTranformer.Domain.Interfaces.ServiceInterfaces
{
    public interface IJsonService
    {
        bool InsertJsonString(string cadena);
    }
}
