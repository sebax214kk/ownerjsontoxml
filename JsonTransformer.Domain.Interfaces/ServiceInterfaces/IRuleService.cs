﻿namespace JsonTranformer.Domain.Interfaces.ServiceInterfaces
{
    public interface IRuleService
    {
        bool InsertRule(string json);

        bool DeleteRules();
    }
}
