﻿namespace JsonTranformer.Domain.Interfaces.ApplicationInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using JsonTranformer.Domain.Entities.Models;

    public interface IMailHelper
    {
        /// <summary>
        /// Enviar un email
        /// </summary>
        /// <param name="mailConfigurationModel"></param>
        /// <param name="recipient"></param>
        /// <param name="subject"></param>
        /// <param name="isHtml"></param>
        /// <param name="body"></param>
        void EnviarMensaje(MailConfigurationModel mailConfigurationModel, string recipient, string subject, bool isHtml, string body);

        /// <summary>
        /// Construye el modelo de configuración para el envío de email
        /// </summary>
        /// <returns></returns>
        MailConfigurationModel setConfigurationMail();

        /// <summary>
        /// Retorna una lista de OwnerModel
        /// </summary>
        /// <param name="dataset"></param>
        /// <returns></returns>
        List<OwnerModel> TransformerToModel(DataSet dataset);

        /// <summary>
        /// Obtiene un Propietario
        /// </summary>
        /// <param name="dataset"></param>
        /// <returns></returns>
        OwnerModel GetOwner(DataSet dataset);
    }
}
