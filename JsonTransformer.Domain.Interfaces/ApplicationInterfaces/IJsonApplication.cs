﻿namespace JsonTranformer.Domain.Interfaces.ApplicationInterfaces
{
    using JsonTranformer.Domain.Entities.Models;

    public interface IJsonApplication
    {
        /// <summary>
        /// Inserta la estructura Json
        /// </summary>
        /// <param name="jsonStructureModel"></param>
        /// <returns></returns>
        bool InsertJson(JsonStructureModel jsonStructureModel);
    }
}
