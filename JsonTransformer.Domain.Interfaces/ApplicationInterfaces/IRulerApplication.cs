﻿namespace JsonTranformer.Domain.Interfaces.ApplicationInterfaces
{
    using System.Data;
    using JsonTranformer.Domain.Entities.Models;

    public interface IRulerApplication
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rulerModel"></param>
        /// <returns></returns>
        bool InsertRule(RulerModel rulerModel);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        bool DeleteRules();

        /// <summary>
        /// Obtiene todas las reglas
        /// </summary>
        /// <returns></returns>
        DataSet GetAllRules();
    }
}
