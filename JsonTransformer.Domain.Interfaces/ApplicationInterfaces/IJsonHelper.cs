﻿namespace JsonTranformer.Domain.Interfaces.ApplicationInterfaces
{
    using JsonTranformer.Domain.Entities.Models;

    public interface IJsonHelper
    {
        /// <summary>
        /// Recibe una cadena y retorna un modelo JsonStructureModel
        /// </summary>
        /// <param name="cadena"></param>
        /// <returns></returns>
        JsonStructureModel MapperJsonStructureModel(string cadena);

        /// <summary>
        /// Transformar un objeto a formato XML
        /// </summary>
        /// <param name="objectNew"></param>
        /// <returns></returns>
        string GetXMLFromObject(object objectNew);
    }
}
