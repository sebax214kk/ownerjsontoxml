﻿namespace JsonTranformer.Domain.Interfaces.ApplicationInterfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using JsonTranformer.Domain.Entities.Models;

    public interface IRulerHelper
    {
        /// <summary>
        /// Recibe unba cadena y lo deserializa a un RulerModel
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        RulerModel MapperJsonToModel(string json);


        /// <summary>
        /// Valida las reglas contra un propietario
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        string ValidateRuler(OwnerModel owner);
    }
}
