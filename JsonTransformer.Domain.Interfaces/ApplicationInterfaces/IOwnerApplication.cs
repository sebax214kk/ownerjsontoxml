﻿namespace JsonTranformer.Domain.Interfaces.ApplicationInterfaces
{
    using System.Collections.Generic;
    using JsonTranformer.Domain.Entities.Models;

    public interface IOwnerApplication
    {
        List<OwnerModel> GetOwnerAll(string rowCountPerPage, string pageIndex);

        OwnerModel GetOwnerByIdentification(string identification);
    }
}
