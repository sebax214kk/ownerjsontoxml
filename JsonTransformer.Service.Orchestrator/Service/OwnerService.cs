﻿namespace JsonTransformer.Service.Orchestrator.Service
{
    using System;
    using System.Collections.Generic;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTranformer.Domain.Interfaces.ServiceInterfaces;
    using JsonTransformer.Domain.Entities.Enum;
    using JsonTransformer.Domain.Entities.Helper;
    using JsonTransformer.Domain.Entities.Logger;

    public class OwnerService : IOwnerService
    {
        public IOwnerApplication OwnerApplication { get; set; }
        
        public OwnerService(IOwnerApplication ownerApplication) {
            this.OwnerApplication = ownerApplication;
        }

        public List<OwnerModel> GetAllOwners(string rowCountPerPage, string pageIndex)
        {
            Logger.Current.Debug("Inicio GetAllOwners");

            try
            {
                Logger.Current.Debug("Obteniendo datos GetAllOwners");

                if (OwnerApplication.GetOwnerAll(rowCountPerPage, pageIndex).Count < 1)
                {
                    throw new TransformerException("GetAllOwners", ErrorMessagesEnum.ValueCanNotOwner);
                }

                return OwnerApplication.GetOwnerAll(rowCountPerPage, pageIndex);
            }
            catch (TransformerException exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new TransformerException("GetAllOwners", ErrorMessagesEnum.UnexpectedError, ex);
            }
        }
    }
}
