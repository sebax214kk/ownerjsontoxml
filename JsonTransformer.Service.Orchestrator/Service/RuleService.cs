﻿namespace JsonTransformer.Service.Orchestrator.Service
{
    using System;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTranformer.Domain.Interfaces.ServiceInterfaces;
    using JsonTransformer.Domain.Entities.Enum;
    using JsonTransformer.Domain.Entities.Helper;
    using JsonTransformer.Domain.Entities.Logger;

    public class RuleService : IRuleService
    {
        public IRulerApplication RulerApplication { get; set; }
        public IRulerHelper RulerHelper { get; set; }

        public RuleService (IRulerApplication rulerApplication, IRulerHelper rulerHelper)
        {
            this.RulerApplication = rulerApplication;
            this.RulerHelper = rulerHelper;
        }

        public bool InsertRule(string json)
        {
            Logger.Current.Debug("Inicio InsertRule");

            try
            {
                Logger.Current.Debug("Serializando InsertRule");

                RulerModel rulerModel = RulerHelper.MapperJsonToModel(json);

                Logger.Current.Debug("almacenando en base de datos InsertRule");

                return RulerApplication.InsertRule(rulerModel);

            }
            catch (TransformerException exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new TransformerException("InsertRule", ErrorMessagesEnum.RulerInsertError, ex);
            }
        }

        public bool DeleteRules()
        {
            return RulerApplication.DeleteRules();
        }
    }
}
