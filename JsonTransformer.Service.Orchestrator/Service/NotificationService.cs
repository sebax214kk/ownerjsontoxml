﻿namespace JsonTransformer.Service.Orchestrator.Service
{
    using System;
    using System.Configuration;
    using JsonTranformer.Domain.Entities.Models;
    using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
    using JsonTranformer.Domain.Interfaces.ServiceInterfaces;
    using JsonTransformer.Domain.Entities.Enum;
    using JsonTransformer.Domain.Entities.Helper;
    using JsonTransformer.Domain.Entities.Logger;

    public class NotificationService : INotificationService
    {
        public IMailHelper MailHelper { get; set; }
        public IJsonHelper JsonHelper { get; set; }
        public IOwnerApplication OwnerApplication { get; set; }
        public IRulerHelper RulerHelper { get; set; }

        public NotificationService(IMailHelper mailHelper, IOwnerApplication ownerApplication, IRulerHelper rulerHelper, IJsonHelper jsonHelper)
        {
            this.MailHelper = mailHelper;
            this.OwnerApplication = ownerApplication;
            this.JsonHelper = jsonHelper;
            this.RulerHelper = rulerHelper;
        }

        public bool SendEmail(string identification)
        {
            Logger.Current.Debug("Inicio SendEmail");

            try
            {
                Logger.Current.Debug("procesando GetOwnerByIdentification");

                OwnerModel owner = this.OwnerApplication.GetOwnerByIdentification(identification);

                if (owner.Equals(null))
                {
                    throw new TransformerException("GetOwnerByIdentification", ErrorMessagesEnum.GetOwnerError);
                }

                Logger.Current.Debug("procesando ValidateRuler");

                string objectXml = this.RulerHelper.ValidateRuler(owner);

                Logger.Current.Debug("procesando EnviarMensaje");

                var configuration = this.MailHelper.setConfigurationMail();
                this.MailHelper.EnviarMensaje(configuration,
                    ConfigurationManager.AppSettings["SmtpFrom"],
                    "Informacion del Propietario en XML",
                    false,
                    objectXml);

                Logger.Current.Debug("Fin SendEmail");

                return true;
            }
            catch (TransformerException exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                Logger.Current.Error("Error SendEmail ", ex);
                throw new TransformerException("SendEmail", ErrorMessagesEnum.SendEmailError, ex);
            }
        }
    }
}
