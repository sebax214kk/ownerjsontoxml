﻿using System;
using JsonTranformer.Domain.Entities.Models;
using JsonTranformer.Domain.Interfaces.ApplicationInterfaces;
using JsonTranformer.Domain.Interfaces.ServiceInterfaces;
using JsonTransformer.Domain.Entities.Enum;
using JsonTransformer.Domain.Entities.Helper;

namespace JsonTransformer.Service.Orchestrator.Service
{
    public class JsonService : IJsonService
    {
        public IJsonApplication JsonApplication { get; set; }
        public IJsonHelper JsonHelper { get; set; }

        public JsonService(IJsonApplication jsonApplication, IJsonHelper jsonHelper)
        {
            this.JsonApplication = jsonApplication;
            this.JsonHelper = jsonHelper;
        }

        public bool InsertJsonString(string cadena)
        {
            try
            {
                JsonStructureModel jsonStuctureModel=  JsonHelper.MapperJsonStructureModel(cadena);

                JsonApplication.InsertJson(jsonStuctureModel);

                return true;
            }
            catch (TransformerException exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new TransformerException("InsertJsonString", ErrorMessagesEnum.TransformerJsonError, ex);
            }
        }

    }
}
